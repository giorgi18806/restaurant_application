<?php

namespace App\Exports;

use App\Sale;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SaleReportExport implements FromView
{
    private $dateStart;
    private $dateEnd;
    private $sales;
    private $totalSale;
    public function __construct($dateStart, $dateEnd)
    {
        $this->dateStart = date("Y-m-d H:i:s", strtotime($dateStart));
        $this->dateEnd = date("Y-m-d H:i:s", strtotime($dateEnd));
        $this->sales = Sale::whereBetween('updated_at', [$dateStart, $dateEnd])->where('sale_status', 'paid')->get();
        $this->totalSale = $this->sales->sum('total_price');
    }

    public function view(): View
    {
        return view('export.sale-report', [
            'sales' => $this->sales,
            'total_sale' => $this->totalSale,
            'date_start' => $this->dateStart,
            'date_end' => $this->dateEnd,
        ]);
    }
}
