<?php

namespace App\Http\Controllers\Management;

use App\Category;
use App\Http\Controllers\Controller;
use App\Menu;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $menus = Menu::paginate(10);
        return view('management.menu.index', compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $categories = Category::all();
        return view('management.menu.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:menus|max:255',
            'price' => 'required|numeric',
            'category_id' => 'required|numeric',
        ]);

        $imageName = 'noimage.png';
        if($request->image) {
            $request->validate([
                'image' => 'nullable|file|image|mimes:jpeg, png, jpg|max:5000',
            ]);
            $imageName = date('mdYHis').uniqid() . '.' . $request->image->extension();
            $request->image->move(public_path('images/menu'), $imageName);
        }
        $menu = new Menu();
        $menu->name = $request->name;
        $menu->price = $request->price;
        $menu->image = $imageName;
        $menu->description = $request->description;
        $menu->category_id = $request->category_id;
        $menu->save();
        session()->flash('status', 'Menu has been created successfully');
        return redirect()->route('menu.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Menu $menu
     * @return Application|Factory|View
     */
    public function edit(Menu $menu)
    {
        $categories = Category::all();
        return view('management.menu.edit', compact('menu', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Menu $menu
     * @return RedirectResponse
     */
    public function update(Request $request, Menu $menu)
    {
        $request->validate([
            'name' => 'required|max:255',
            'price' => 'required|numeric',
            'category_id' => 'required|numeric',
        ]);

        if($request->image) {
            $request->validate([
                'image' => 'nullable|file|image|mimes:jpeg, png, jpg|max:5000',
            ]);
            if($menu->image != 'noimage.png') {
                $imageName = $menu->image;
                unlink(public_path('images/menu') . '/' . $imageName);
            }
            $imageName = date('mdYHis').uniqid() . '.' . $request->image->extension();
            $request->image->move(public_path('images/menu'), $imageName);
        }else {
            $imageName = $menu->image;
        }

        $menu->name = $request->name;
        $menu->price = $request->price;
        $menu->image = $imageName;
        $menu->description = $request->description;
        $menu->category_id = $request->category_id;
        $menu->save();
        session()->flash('status', 'Menu has been created successfully');
        return redirect()->route('menu.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        if($menu->image != 'noimage.png') {
            unlink(public_path('images/menu') . '/' . $menu->image);
        }
        $menu->delete();
        session()->flash('status', 'Menu has been deleted successfully');
        return redirect()->route('menu.index');
    }
}
