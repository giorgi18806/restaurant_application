<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use App\Table;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $tables = Table::paginate(2);
        return view('management.table.index', compact('tables'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('management.table.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:tables|max:255',
        ]);
        $table = new Table();
        $table->name = $request->name;
        $table->save();
        session()->flash('status', 'Table has been created successfully');
        return redirect()->route('tables.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Table $table)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Table $table
     * @return Application|Factory|View
     */
    public function edit(Table $table)
    {
        return view('management.table.edit', compact('table'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Table $table
     * @return RedirectResponse
     */
    public function update(Request $request, Table $table)
    {
        $request->validate([
            'name' => 'required|unique:tables|max:255',
        ]);
        $table->name = $request->name;
        $table->save();
        session()->flash('status', 'Table has been updated successfully');
        return redirect()->route('tables.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Table $table
     * @return void
     * @throws \Exception
     */
    public function destroy(Table $table)
    {
        $table->delete();
        session()->flash('status', 'Table has been deleted successfully');
        return redirect()->route('tables.index');
    }
}
