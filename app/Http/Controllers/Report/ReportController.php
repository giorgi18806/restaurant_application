<?php

namespace App\Http\Controllers\Report;

use App\Exports\SaleReportExport;
use App\Http\Controllers\Controller;
use App\Sale;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    public function index()
    {
        return view('report.index');
    }

    public function show(Request $request)
    {
        $request->validate([
            'date_start' => 'required',
            'date_end' => 'required',
        ]);

        $dateStart = date("Y-m-d H:i:s", strtotime($request->date_start . '00:00:00'));
        $dateEnd = date("Y-m-d H:i:s", strtotime($request->date_end . '23:59:59'));

        $sales = Sale::whereBetween('updated_at', [$dateStart, $dateEnd])->where('sale_status', 'paid');

        return view('report.show')->with('dateStart', $dateStart)->with('dateEnd', $dateEnd)
            ->with('totalSale', $sales->sum('total_price'))->with('sales', $sales->paginate(3));
    }

    public function export(Request $request)
    {
        return Excel::download(new SaleReportExport($request->date_start, $request->date_end), 'saleReport.xlsx');
    }
}
