@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-start">
            @include('management.inc.sidebar')
            <div class="col-md-8">
                <i class="fas fa-align-justify mr-1"></i>Create a Category
                <a href="{{ route('categories.index') }}" class="btn btn-warning btn-sm float-right"><i class="fa-solid fa-angle-left"></i>Back</a>
                <hr>
                <form action="{{ route('categories.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="categoryName">Category Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Category...">
                        @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
