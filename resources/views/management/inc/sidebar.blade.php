<div class="col-md-4">
    <div class="list-group">
        <a href="{{ route('categories.index') }}" class="list-group-item list-group-item-action">
            <i class="fas fa-align-justify mr-1"></i>Category
        </a>
        <a href="{{ route('menu.index') }}" class="list-group-item list-group-item-action">
            <i class="fas fa-hamburger mr-1"></i>Menu
        </a>
        <a href="{{ route('tables.index') }}" class="list-group-item list-group-item-action">
            <i class="fas fa-chair mr-1"></i>Table
        </a>
        <a href="{{ route('users.index') }}" class="list-group-item list-group-item-action">
            <i class="fas fa-user-cog mr-1"></i>User
        </a>
    </div>
</div>
