@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-start">
            @include('management.inc.sidebar')
            <div class="col-md-8">
                <i class="fas fa-hamburger mr-1"></i>Create New Menu
                <a href="{{ route('menu.index') }}" class="btn btn-warning btn-sm float-right"><i class="fa-solid fa-angle-left"></i>Back</a>
                <hr>
                <form action="{{ route('menu.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="menuName">Menu Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Menu...">
                        @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div>
                        <label for="menuPrice">Price</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">$</span>
                            </div>
                            <input type="text" name="price" class="form-control"
                                   aria-label="Amount (to the nearest dollar">
                            <div class="input-group-append">
                                <span class="input-group-text">.00</span>
                            </div>
                        </div>
                    </div>
                    <div>
                        <label for="menuImage">Image</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Upload</span>
                            </div>
                            <div class="custom-file">
                                <input type="file" name="image" class="custom-file-input"
                                       id="inputGroupFile01">
                                <label for="inputGroupFile01" class="custom-file-label">Choose File</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="menuDescription">Menu Description</label>
                        <input type="text" name="description" class="form-control" placeholder="Description...">
                        @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="menuCategory">Menu Category</label>
                        <select name="category_id" id="category_id" class="form-control">
                            <option>Select category...</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                        @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
