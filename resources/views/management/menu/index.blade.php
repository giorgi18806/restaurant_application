@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-start">
            @include('management.inc.sidebar')
            <div class="col-md-8">
                <i class="fas fa-hamburger mr-1"></i>Menu
                <a href="{{ route('menu.create') }}" class="btn btn-success btn-sm float-right"><i class="fas fa-plus mr-1"></i>Create New Menu</a>
                <hr>
                @if(Session()->has('status'))
                    <div class="alert alert-success" role="alert">{{ Session()->get('status') }}</div>
                @endif
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Menu Name</th>
                            <th scope="col">Price</th>
                            <th scope="col">Picture</th>
                            <th scope="col">Description</th>
                            <th scope="col">Category</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($menus as $menu)
                            <tr>
                                <th scope="row">{{ $menu->id }}</th>
                                <td>{{ $menu->name }}</td>
                                <td>{{ $menu->price }}</td>
                                <td>
                                    <img src="{{ asset('images/menu') }}/{{ $menu->image }}" alt="{{ $menu->name }}" width="120px" height="120px" class="img-thumbnail">
                                </td>
                                <td>{{ $menu->description }}</td>
                                <td>{{ $menu->category->name }}</td>
                                <td>
                                    <a href="{{ route('menu.edit', $menu) }}" class="btn btn-warning">Edit</a>
                                </td>
                                <td>
                                    <form action="{{ route('menu.destroy', $menu) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $menus->links() }}
            </div>
        </div>
    </div>
@endsection
