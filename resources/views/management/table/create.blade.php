@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-start">
            @include('management.inc.sidebar')
            <div class="col-md-8">
                <i class="fas fa-chair mr-1"></i>Create New Table
                <a href="{{ route('tables.index') }}" class="btn btn-warning btn-sm float-right"><i class="fa-solid fa-angle-left"></i>Back</a>
                <hr>
                <form action="{{ route('tables.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="tableName">Table Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Table...">
                        @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
