@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-start">
            @include('management.inc.sidebar')
            <div class="col-md-8">
                <i class="fas fa-chair mr-1"></i>Table
                <a href="{{ route('tables.create') }}" class="btn btn-success btn-sm float-right"><i class="fas fa-plus mr-1"></i>Create New Table</a>
                <hr>
                @if(Session()->has('status'))
                    <div class="alert alert-success" role="alert">{{ Session()->get('status') }}</div>
                @endif
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Table</th>
                            <th scope="col">Status</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tables as $table)
                            <tr>
                                <th scope="row">{{ $table->id }}</th>
                                <td>{{ $table->name }}</td>
                                <td>{{ $table->status }}</td>
                                <td>
                                    <a href="{{ route('tables.edit', $table) }}" class="btn btn-warning">Edit</a>
                                </td>
                                <td>
                                    <form action="{{ route('tables.destroy', $table) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $tables->links() }}
            </div>
        </div>
    </div>
@endsection
