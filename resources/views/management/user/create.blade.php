@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-start">
            @include('management.inc.sidebar')
            <div class="col-md-8">
                <i class="fas fa-user mr-1"></i>Create New User
                <a href="{{ route('users.index') }}" class="btn btn-warning btn-sm float-right"><i class="fa-solid fa-angle-left"></i>Back</a>
                <hr>
                <form action="{{ route('users.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" placeholder="User...">
                        @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" placeholder="Email...">
                        @error('email') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="text" name="password" class="form-control" placeholder="Password...">
                        @error('password') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="role">Role</label>
                        <select name="role" id="role" class="form-control">
                            <option value="" selected disabled>Select role</option>
                            <option value="admin">Admin</option>
                            <option value="cashier">Cashier</option>
                        </select>
                        @error('email') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
