@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-start">
            @include('management.inc.sidebar')
            <div class="col-md-8">
                <i class="fas fa-users mr-1"></i>Users
                <a href="{{ route('users.create') }}" class="btn btn-success btn-sm float-right"><i class="fas fa-plus mr-1"></i>Create New User</a>
                <hr>
                @if(Session()->has('status'))
                    <div class="alert alert-success" role="alert">{{ Session()->get('status') }}</div>
                @endif
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Role</th>
                            <th scope="col">Email</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <th scope="row">{{ $user->id }}</th>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->role }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    <a href="{{ route('users.edit', $user) }}" class="btn btn-warning">Edit</a>
                                </td>
                                <td>
                                    <form action="{{ route('users.destroy', $user) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $users->links() }}
            </div>
        </div>
    </div>
@endsection
