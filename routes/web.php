<?php

//use App\Http\Controllers\Management\CategoryController;

Route::get('/', 'HomeController@index');

Auth::routes(['register' => false, 'reset' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {
    //cashier
    Route::get('/cashier', 'Cashier\CashierController@index')->name('cashier');
    Route::get('/cashier/get-table', 'Cashier\CashierController@getTables')->name('cashier-get-tables');
    Route::get('/cashier/getSaleDetailsByTable/{table_id}', 'Cashier\CashierController@getSaleDetailsByTable')->name('cashier-get-sale-details-by-table');
    Route::get('/cashier/getMenuByCategory/{category_id}', 'Cashier\CashierController@getMenuByCategory')->name('cashier-get-menu-by-category');
    Route::post('/cashier/orderFood', 'Cashier\CashierController@orderFood')->name('cashier-order-food');
    Route::post('/cashier/confirmOrderStatus', 'Cashier\CashierController@confirmOrderStatus');
    Route::post('/cashier/deleteSaleDetail', 'Cashier\CashierController@deleteSaleDetail');
    Route::post('/cashier/increase-quantity', 'Cashier\CashierController@increaseQuantity');
    Route::post('/cashier/decrease-quantity', 'Cashier\CashierController@decreaseQuantity');
    Route::post('/cashier/savePayment', 'Cashier\CashierController@savePayment');
    Route::get('/cashier/showReceipt/{saleID}', 'Cashier\CashierController@showReceipt');

});
Route::middleware(['auth', 'VerifyAdmin'])->group(function () {
//report
    Route::get('/report', 'Report\ReportController@index')->name('report');
    Route::get('/report/show', 'Report\ReportController@show')->name('report-show');

// export to excel
    Route::get('/report/show/export', 'Report\ReportController@export')->name('report-export');

    Route::get('/management', function() {
        return view('management/index');
    })->name('management.index');
    Route::resource('management/categories', 'Management\CategoryController');
    Route::resource('management/menu', 'Management\MenuController');
    Route::resource('management/tables', 'Management\TableController');
    Route::resource('management/users', 'Management\UserController');
});
